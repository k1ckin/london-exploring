import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.sql.SparkSession
import java.io.File


fun getSparkSession(): SparkSession {
    val conf = SparkConf()
        .setMaster("local[*]")
        .setAppName("New York Taxi Fare Prediction")
        .set("spark.driver.memory","2G")
        .set("spark.sql.crossJoin.enabled", "true")
        .set("spark.network.timeout", "10000")
        .set("spark.driver.maxResultSize", "2G")
        .set("spark.memory.offHeap.size","12g")
        .set("spark.memory.offHeap.enabled","true")
        .setExecutorEnv("spark.executor.memoryOverhead","1G")
        .set("spark.storage.memoryFraction", "0.1")
    val context = SparkContext(conf)
    return SparkSession(context)
}

fun extractResultFromFolder(path:String){
    val folder = File("$path.folder")
    val result = File(folder.path+"/part-00000")
    result.copyTo(File(path),true)
    folder.deleteRecursively()
}