package smilen.project

import org.apache.spark.sql.Encoders
import org.joda.time.DateTime
import org.joda.time.Instant
import java.io.Serializable
import java.util.*

class DataPoint () : Serializable{

    constructor(lat: String, lon: String, stamp: String, convertCoords: Boolean = true):this(){
        latitude =  if (convertCoords)  convertE7StringToDouble(lat) else lat.toDouble()
        longitude = if (convertCoords)  convertE7StringToDouble(lon) else lon.toDouble()
        timestamp = convertMilisecondTimestampToDateTime(stamp)
        dateJSON = "${timestamp.year+1900}|${if (timestamp.month+1<10) "0" else ""}${timestamp.month+1}|${if (timestamp.date<10) "0" else ""}${timestamp.date}"
        weekend = timestamp.day == 3 || timestamp.day == 4
    }

    var latitude: Double = 0.1
    var longitude: Double = 0.1
    var dateJSON : String = "undefined"
    var weekend: Boolean = false
     lateinit var timestamp: Date

    fun isAfter(dt:DateTime) = DateTime(timestamp).isAfter(dt)
    private fun convertE7StringToDouble(e7:String):Double = roundToDecimals(e7.toDouble()/(10* 1000 * 1000),4)
    private fun convertMilisecondTimestampToDateTime(s:String): Date = Date(s.toLong())
    private fun roundToDecimals(number: Double, numDecimalPlaces: Int): Double {
        val factor = Math.pow(10.0, numDecimalPlaces.toDouble())
        val digit4 = Math.round(number * factor) / factor
//        val uneven = (factor * digit4 ) %2 == 1.0
//        val digit4Rounded = if (uneven) digit4 + 0.0001 else digit4
        return digit4
    }
    // 51.4421 vs 51.4422

    override fun toString(): String {
        return "{\"latitude\":$latitude, \"longitude\":$longitude, \"date\":\"$dateJSON\", \"weekend\": $weekend}"
    }
}

val dataPointEncoder = Encoders.bean(DataPoint::class.java)