import org.apache.spark.api.java.function.MapFunction
import org.apache.spark.api.java.function.Function
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Encoders
import org.joda.time.DateTime
import smilen.project.DataPoint
import smilen.project.RawDataPoint
import smilen.project.rawDataPointEncoder
import smilen.project.rawLocationSchema
import smilen.project.dataPointEncoder
import java.util.*
import org.apache.commons.lang3.StringUtils;
import org.apache.spark.sql.Row
import scala.Tuple2
import java.io.File
import org.apache.spark.sql.functions.min


// TODO(Change parent package name)
fun main(args: Array<String>){
    val datapoints = `Read raw data with spark`()
    val sizeAllDataPoints = datapoints.count()
    val onlyLondon = `Filter out dataset`(datapoints)
    val sizeOnlyLondonDataPoints = onlyLondon.count();
    print("${sizeAllDataPoints} vs ${sizeOnlyLondonDataPoints}")
    `Save dataset to file`(onlyLondon)
}

val dataFolder = "data/"
val appDataFolder = "app/src/data/"
val rawData = "${dataFolder}locations.json"
val filteredJSON = "${dataFolder}filtered.json"
val appFiltreredJSON = "${appDataFolder}filtered.json"

private val spark = getSparkSession()

private fun `Read raw data with spark`(): Dataset<DataPoint> {
    val raw = spark.read()
        .option("multiline",true)
        .schema(rawLocationSchema)
        .json(rawData)
    val rawDataPoint = raw.`as`(rawDataPointEncoder);
    val filteredRawDatapoints = `Filter raw dataset for blanks`(rawDataPoint)

    val datapoints = filteredRawDatapoints.map(MapFunction{r : RawDataPoint -> DataPoint(r.latitudeE7,r.longitudeE7,r.timestampMs) }, dataPointEncoder )
    return datapoints
}

private fun `Filter raw dataset for blanks`(ds: Dataset<RawDataPoint>) : Dataset<RawDataPoint>{
    return ds.filter { r ->
        r.timestampMs.length == 13
    }
}


private fun `Filter out dataset`(ds:Dataset<DataPoint>):Dataset<DataPoint>{
    val movedToLondon = DateTime(2016,9,29,0,0,0)
    val onlyLondon = ds.filter{
        (it.latitude in 51.46..51.54)
        && (it.longitude >=-0.2 && it.longitude <= 0.02)
        && (it.isAfter(movedToLondon))
    }
    val grouped = onlyLondon.groupBy("longitude", "latitude").agg(min("timestamp"))
    val result = grouped
        .withColumnRenamed("min(timestamp)", "timestamp")
        .map(MapFunction{r : Row -> DataPoint(r.getDouble(1).toString(),r.getDouble(0).toString(), r.getStruct(2).getLong(5).toString(), false) }, dataPointEncoder )
    val sorted =  result.orderBy("dateJSON")
    return sorted
}

private fun `Save dataset to file`(ds:Dataset<DataPoint>){
    val count = ds.count()
    ds
        .coalesce(1)
        .toJavaRDD()
        .zipWithIndex()
        .map( Function<Tuple2<DataPoint, Long>, String>{ tuple: Tuple2<DataPoint,Long> ->
            val t = tuple._1
            val i = tuple._2
            return@Function if (i == 0.toLong())  "[\n" + t.toString() + ","
            else
                (if (i == count-1)  t.toString() + "\n]"
                else t.toString() + ",")}
        )
        .saveAsTextFile("$filteredJSON.folder")
    extractResultFromFolder(filteredJSON);
    val result = File(filteredJSON)
    val appDataFile = File(appFiltreredJSON)
    result.copyTo(appDataFile,true)
}