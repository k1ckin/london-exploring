package smilen.project

import org.apache.spark.sql.Encoders
import org.apache.spark.sql.types.DataTypes
import org.apache.spark.sql.types.StructType
import java.io.Serializable


val rawLocationSchema =  StructType()
    .add("latitudeE7", DataTypes.StringType)
    .add("longitudeE7", DataTypes.StringType)
    .add("timestampMs", DataTypes.StringType)

class RawDataPoint() : Serializable{
    lateinit var latitudeE7 : String
    lateinit var longitudeE7 : String
    lateinit var timestampMs: String
}

val rawDataPointEncoder = Encoders.bean(RawDataPoint::class.java)