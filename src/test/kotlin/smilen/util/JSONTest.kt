package smilen.util

import org.junit.Test
import getSparkSession
import org.apache.spark.sql.Encoders
import org.apache.spark.sql.types.DataTypes
import org.apache.spark.sql.types.StructType
import scala.math.BigInt
import java.io.Serializable
import java.math.BigInteger


class JSONTest{

    private val spark = getSparkSession()

    @Test
    fun `Generic - import JSON using Spark`(){
        val people = spark.read()
            .option("multiline",true)
            .schema(genericSchema)
            .json("src/test/resources/generic.json")
        val asPerson = people.`as`(testEncoder)
        val person1 = asPerson.first()
        assert(person1.nm == "Edward the Elder")
    }
}

internal class Person () : Serializable{

    var id: Int = 0
    lateinit var nm: String
    lateinit var cty: String
    lateinit var hse: String
    lateinit var yrs: String
}

private val testEncoder = Encoders.bean(Person::class.java);

internal val genericSchema =   StructType()
.add("id", DataTypes.IntegerType)
.add("nm", DataTypes.StringType)
.add("cty",DataTypes.StringType)
.add("hse", DataTypes.StringType)
.add("yrs", DataTypes.StringType)