import org.junit.Test


class ProjectTest {

    @Test
    fun `Stich E7 Double from string should yield expected`(){
        val e7  = "420150970"

        val result = (e7.substring(0,2) + "." + e7.substring(2, e7.length)).toDouble()
        assert (result == 42.0150970)
    }

}