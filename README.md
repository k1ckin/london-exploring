# About

Inspired by a Reddit post where a user downloaded his Google phone location history over the span of a few years and mapped data points overlayed on top of a map of a city. 
I decided to do the following with the ~2 years of data I had of me going about in London. Served as an excercise for iterating through JSON data in Apache Spark.

# Tech used 
- Kotlin 
- Apache Spark
- React + Typescript
- Maps library

# End result

![Gif of end result](https://thumbs.gfycat.com/LargeZealousFlounder-size_restricted.gif)