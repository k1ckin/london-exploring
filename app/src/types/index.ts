export interface LocationHistory {
  name: string;
  datapoints: DataPoint[];
}

export interface DataPoint {
  latitude: number;
  longitude: number;
  date: Date;
  weekend: boolean;
}

export type IncomingDataPoint = {
  longitude: number;
  latitude: number;
  date: string;
  weekend: boolean;
};
