import * as React from "react";
import { DataPoint } from "../../types";
import ReactMapboxGl, { Layer, Feature } from "react-mapbox-gl";

interface Props {
  points: DataPoint[];
}

interface State {
  currentDate: Date;
  running: boolean;
  monthYear: string;
  latestIndex: number;
  prevousIndex: number;
}

const dateContainer = {
  position: "absolute",
  bottom: "60px",
  left: "100px",
  fontSize: "56px",
  color: "#fd6a02",
  textShadow: " 1px 2px #aaa",
  zIndex: 1000
} as React.CSSProperties;

export default class Points extends React.PureComponent<Props, State> {
  constructor(props: Props) {
    super(props);
  }

  months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ];

  componentDidMount() {
    this.startTimer((this.props.points && this.props.points.length) || 0);
  }

  state: State = {
    currentDate: new Date(2016, 9),
    running: false,
    prevousIndex: 0,
    latestIndex: 0,
    monthYear: ""
  };

  getStringForDate = (dd: Date): string => {
    const maxDate = new Date(dd.getTime());
    maxDate.setMonth(maxDate.getMonth() - 2);
    const year = maxDate.getFullYear();
    const month = this.months[maxDate.getMonth()];
    return `${month} - ${year}`;
  };

  startTimer = (maxIterations: number) => {
    console.log(`Starting timer - 'running' is ${this.state.running}`);
    const { running } = this.state;
    if (running) return;
    else this.setState({ running: true });

    const interval = setInterval(() => {
      this.setState(
        (oldState: State) => {
          const nextMonth = new Date(oldState.currentDate.getTime());
          nextMonth.setMonth(nextMonth.getMonth() + 1);
          const latestIndex = this.props.points.findIndex(
            d => d.date.getTime() >= nextMonth.getTime()
          );
          return {
            currentDate: nextMonth,
            monthYear: this.getStringForDate(nextMonth),
            latestIndex,
            prevousIndex: oldState.latestIndex
          };
        },
        () => {
          if (this.state.currentDate.getTime() > new Date().getTime()) {
            clearInterval(interval);
            this.setState({ running: false });
          }
        }
      );
    }, 5100);
  };

  render() {
    const datapoints = this.props.points;
    if (!datapoints.length) return null;
    const { currentDate, monthYear, prevousIndex, latestIndex } = this.state;

    const filtered = datapoints.slice(0, latestIndex);
    const allFeatures = filtered.map((d, i) => {
      return (
        <Feature
          coordinates={[d.longitude, d.latitude]}
          key={`${d.longitude}${d.latitude}${d.date}`}
          properties={{ display: "none" }}
        />
      );
    });

    console.log(`Previous index: ${prevousIndex}, new Index: ${latestIndex}`);
    const oldFeatures = allFeatures.slice(0, prevousIndex);
    const newFeatures = allFeatures.slice(prevousIndex, latestIndex);

    return (
      <>
        <Layer
          type="circle"
          id="marker"
          paint={{
            "circle-radius": 3,
            "circle-color": "#fd6a02",
            "circle-opacity": 1
          }}
        >
          {newFeatures}
        </Layer>
        <Layer
          type="circle"
          id="another"
          paint={{
            "circle-radius": 2,
            "circle-color": "#aa8a49",
            "circle-opacity": 0.8
          }}
        >
          {oldFeatures}
        </Layer>

        <div style={dateContainer}>{this.state.monthYear}</div>
      </>
    );
  }
}
