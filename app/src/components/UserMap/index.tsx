import * as React from "react";
import { LocationHistory } from "../../types";

import ReactMapboxGl, { Layer, Feature } from "react-mapbox-gl";
import { Viewport } from "csstype";
import Points from "./points";

const uuid = require("uuid/v4");

interface Props {
  locationHistory?: LocationHistory;
}

interface State {
  numPoints: number;
}

const mapConfig = {
  center: [51.47, -0.2],
  zoom: 1
};

const Map = ReactMapboxGl({
  bearingSnap: 0,
  dragPan: true,
  scrollZoom: true,
  boxZoom: true,
  accessToken:
    "pk.eyJ1Ijoic21pbGVucyIsImEiOiJPOWFvdklzIn0.1kMfrMRLjK4GiZlH0w0SbQ"
});

export default class UserMap extends React.PureComponent<Props, State> {
  state: State = {
    numPoints: 0
  };

  render() {
    if (
      !this.props.locationHistory ||
      !this.props.locationHistory.datapoints.length
    )
      return <div />;

    const initialSise = this.props.locationHistory!!.datapoints.length;

    const datapoints = this.props.locationHistory!!.datapoints;
    const filteredSize = datapoints.length;
    console.log(`Initial ${initialSise} `);

    return (
      <div className="reactmapgl">
        <Map
          style="mapbox://styles/smilens/cjq3yvrna78mt2sqier7713pe"
          center={[mapConfig.center[1], mapConfig.center[0]]}
          maxBounds={[
            [mapConfig.center[1], mapConfig.center[0]],
            [0.04, 51.539]
          ]}
          containerStyle={{
            height: "970px",
            width: "1680px",
            zoom: mapConfig.zoom
          }}
        >
          <Points points={datapoints} />
        </Map>
      </div>
    );
  }
}
