import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import { LocationHistory, DataPoint, IncomingDataPoint } from "./types";
import UserMap from "./components/UserMap";
const data = require("./data/filtered.json");

class App extends Component {
  readJSON(name: string): LocationHistory {
    const arr = data.map((d: IncomingDataPoint) => {
      const dateParts = d.date.split("|").map(s => parseInt(s));
      const date = new Date(dateParts[0], dateParts[1], dateParts[2]);
      const weekend = d.weekend;

      return {
        latitude: d.latitude,
        longitude: d.longitude,
        date,
        weekend
      } as DataPoint;
    });

    return {
      name,
      datapoints: arr
    } as LocationHistory;
  }

  render() {
    const locationHistory = this.readJSON("Smilen");
    return (
      <div style={{ textAlign: "left" }}>
        <UserMap locationHistory={locationHistory} />
      </div>
    );
  }
}

export default App;
